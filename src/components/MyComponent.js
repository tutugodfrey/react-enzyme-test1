// MyComponent.js
import React from 'react';

export default class MyComponent extends React.Component {
  updateState(event) {
    this.setState({
      input: event.target.value
    })
  }
  render() {
    return (
    <div>
      <form>
        <input
          name="myName"
          onChange={this.updateState.bind(this)}
          type="text"
        />
      </form>
    </div>
    );
  }
}
